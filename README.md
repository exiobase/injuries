# Injuries

## Description
This project is about listing / estimating the number of fatal injuries at work. This project appeared in a continuation of the publication from the [World Health Organization](https://www.who.int/news/item/16-09-2021-who-ilo-almost-2-million-people-die-from-work-related-causes-each-year) stating that almost 2 million people die from work-related causes each year. 

This module **injuries.py** is composed of 3 parts.
- Data have to be download from different sources.
- Data have to be refined
- Calculation of fatal injuries per country / per sex / per year / per EXIOBASE sector.


### Download
In order to set up the final table, we need a certain amount of data.

#### WHO
First, we download the data combined by WHO:

```Python
src_url = url_who()
src_csv = Path("WHO_ILO_BOD_LWH_2021_05.csv")
```
These data are stored in the dataframe called **who**.
We then keep only the one of interest :
- age above 15 years old
- we keep the split by sex (so we remove all data related to the total population)
- we keep the data related to injuries leading to Death
- we keep only the number of death, not the rate or the population attributable fraction.

```Python

who = who[who['age'].str.contains('≥15 years',regex=True)]
who = who[who['sex'].str.contains('Male|Female',regex=True)]
who = who[who['measure'].str.contains('Deaths',regex=True)]
who=who.drop(columns=['Population-attributable fraction (%)','Rate (per 100,000 of population)'])
who = who[who['year'] >1994]

country_code = list(who['country_name_who'])

who.insert(4, 'ISO3', cc_all.convert(names = country_code, to='ISO3'))
who.insert(5, 'EXIO3', cc_all.convert(names = country_code, to='EXIO3'))
who = who[~who['EXIO3'].str.contains('not found',regex=True)]
who=who.drop(columns=['risk_factor','age'])
```
We then end up with the number of death per country and per sex for only 3 years (2000,2010 and 2016).

#### Eurostat
Then, we download data related to fatal accident at work by NACE Rev.2 activity from [Eurostat](https://ec.europa.eu/eurostat/web/main/home):

```Python
src_url2 = "https://ec.europa.eu/eurostat/estat-navtree-portlet-prod/BulkDownloadListing?file=data/hsw_n2_02.tsv.gz"
src_csv2 = Path("hsw_n2_02.tsv")
```

These data are stored in a dataframe called **injuries_fatal**.
The table from Eurostat contains data related to number of death by year, sex and nace activities for european countries.

#### Combining the data
We based our calculation on the values available in **who**. As we stated previously, only 3 years are availables.
We first focus on years 2000 and 2016.

##### 2010 and 2016 
Depending on the country of interest, 2 methods are applied.
For the non european countries we need the workforce associated to each EXIOBASE sector (or Nace Rev. 2 activity). This is found in the table located in the **aux** folder : **aux/table_workforce_by_ISO3.csv**. The data are stored in the dataframe called **workforce**.

_For non european countries :_
1. we get the number of death from **who** 
2. we distribute the number of death propertionnaly over the Nace Rev 2 activity as a funtion of the workforce associated to this Nace Rev 2 activity : 

```Python
injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=round(float(who_aggregate.loc[who_aggregate.index==(year, code, 'Deaths', c, b),'Number (DALYs in 1000s)'].to_string(index=False,header=False))*float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_'+str(a)),'obs_value'].to_string(index=False,header=False))/float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_TOTAL'),'obs_value'].to_string(index=False,header=False)))
```   

where 
```Python
injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']
```
is the number of death per country per Nace Rev 2 activity per sex and per year we are calculating and storing in a new dataframe called **injuries_split_ROW**.

```python
who_aggregate.loc[who_aggregate.index==(year, code, 'Deaths', c, b),'Number (DALYs in 1000s)']
```
is the number of death per country per year (2010 or 2016) per sex and per Nace rev2 activity.

```Python
workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_'+str(a)),'obs_value']
```
is the number of working people per country per year per sex and per EXIOBASE activity.
```Python
workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_TOTAL'),'obs_value']
```
is the total workforce per country, per year per sex, no matter the EXIOBASE activity.

so if A is the number of death per country per sex per year per EXIOBASE activity found in who,
if B is the number of people working per country per sex per year per EXIOBASE activity found in workforce,
if c is the total workforce per country per sex per year,
if D is the number of death per country per sex per year and per EXIOBASE activity,

**D = A * (B/C)**


_For european countries :_
```Python
injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=round(float(who_aggregate.loc[who_aggregate.index==(year, code, 'Deaths', c,   b),'Number (DALYs in 1000s)'].to_string(index=False,header=False))*float(injuries_fatal.loc[(injuries_fatal['unit']=='NR')&(injuries_fatal['nace_r2']==a)&(injuries_fatal['ISO3']==code),str(year)].to_string(index=False,header=False))/float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(year)].to_string(index=False,header=False)))
```

where 
```Python
injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']
```
is the number of death per country per Nace Rev 2 activity per sex and per year we are calculating and storing in a new dataframe called **injuries_split_ROW**.

```python
who_aggregate.loc[who_aggregate.index==(year, code, 'Deaths', c, b),'Number (DALYs in 1000s)']
```
is the number of death per country per year (2010 or 2016) per sex and per Nace rev2 activity.

```Python
injuries_fatal.loc[(injuries_fatal['unit']=='NR')&(injuries_fatal['nace_r2']==a)&(injuries_fatal['ISO3']==code),str(year)]
```
is the number of death per country per sex per year and per Nace Rev2 activity

```Python
injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(year)]
```
is the total number of death per country per sex per year no matter the Nace Rev2 activity.


so if A is the number of death per country per sex per year per EXIOBASE activity found in who,
if B is the death per country per sex per year per EXIOBASE activity found in Eurostat,
if c is the total number of death per country per sex per year found in Eurostat,
if D is the number of death per country per sex per year and per EXIOBASE activity,

**D = A * (B/C)**


##### Years before 2010, between 2011 and 2015 and after 2016 

For the years before 2010, we use the well known values from the WHO database for year 2010. For the years coming after 2010, we choose to take the 2016 values from the WHO database.

_For european countries :_
For the years previous to 2010, we keep 2010 as a reference as 2010 is available in the database who.
We need to evaluate what could have been the value of death per country per sex per year and per Nace rev2 activity (called **unknown_who** in the script). We condider this value should vary like the number of death available in the Eurostat database (injuries_fatal):

```Python
unknown_who = round(float(who_aggregate.loc[who_aggregate.index==(2010, code, 'Deaths', c,   b),'Number (DALYs in 1000s)'].to_string(index=False,header=False)) + ((float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(year)].to_string(index=False,header=False))-float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(2010)].to_string(index=False,header=False)))/(float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(2010)].to_string(index=False,header=False)))))
```
So if the number of death increases by 10% from 2009 to 2010 for a country for a sex category in the Eurostat database, we condider that the number of death in the WHO database between 2009 and 2010 should also increase by 10%.
Having an estimate of the death from the WHO database, we follow the same method as previously in order to split the number of death by Nace Rev2 activity :

```Python
injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=round(unknown_who*float(injuries_fatal.loc[(injuries_fatal['unit']=='NR')&(injuries_fatal['nace_r2']==a)&(injuries_fatal['ISO3']==code),str(year)].to_string(index=False,header=False))/float(injuries_fatal_aggregate.loc[injuries_fatal_aggregate.index==('NR', code),str(year)].to_string(index=False,header=False)))
```

_For non european countries :_
```Python
injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==year),'death']=round((float(injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==2010),'death'].to_string(index=False,header=False)))+(float(injuries_split_ROW.loc[(injuries_split_ROW['ISO3']==code)&(injuries_split_ROW['EXIOBASE sector']==a)&(injuries_split_ROW['sex']==b)&(injuries_split_ROW['estimate']==c)&(injuries_split_ROW['year']==2010),'death'].to_string(index=False,header=False))*((float(workforce.loc[(workforce.ref_area==code)&(workforce.time==year)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_'+str(a)),'obs_value'].to_string(index=False,header=False))-float(workforce.loc[(workforce.ref_area==code)&(workforce.time==2010)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_'+str(a)),'obs_value'].to_string(index=False,header=False)))/(float(workforce.loc[(workforce.ref_area==code)&(workforce.time==2010)&(workforce.sex==b)&(workforce.classif1=='ECO_DETAILS_'+str(a)),'obs_value'].to_string(index=False,header=False))))))                            
```

For the non european counties, we make the variation of death following the variation of the workforce.

### injuries_split_ROW

**injuries_split_ROW** is the whole dataframe where, for each ISO3 code available in the who dataframe, we have for years 2008 to 2019, the number of death for each sex and each EXIOBASE sector. 


                                




